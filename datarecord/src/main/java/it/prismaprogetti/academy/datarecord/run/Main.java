package it.prismaprogetti.academy.datarecord.run;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import it.prismaprogetti.academy.datarecord.DataField;
import it.prismaprogetti.academy.datarecord.DataRecord;
import it.prismaprogetti.academy.datarecord.DataRecordException;
import it.prismaprogetti.academy.datarecord.impl.FileCsvDataRecordDecoder;
import it.prismaprogetti.academy.datarecord.impl.FileCsvDataRecordDecoderSelector;

public class Main {

	public static void main(String[] args)
			throws DataRecordException, IOException, NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {

		String pathInLineFileDati = "C:\\Users\\Flavio Giannini\\git\\datarecord\\datarecord\\resources\\inlinedata.txt";
		//String pathMultiLineFileDati = "C:\\Users\\Flavio Giannini\\git\\datarecord\\datarecord\\resources\\data.txt";

		/*
		 * Selezionatore di decoder
		 */
		FileCsvDataRecordDecoderSelector selector = FileCsvDataRecordDecoderSelector.builder();

		FileCsvDataRecordDecoder fileCsvDataRecordDecoder = selector.select(pathInLineFileDati);


		while (fileCsvDataRecordDecoder.hasNext()) {

			DataRecord dataRecord = fileCsvDataRecordDecoder.nextRecord();

			System.out.println("--->" + dataRecord.toString());

			while (dataRecord.hasNext()) {
				DataField dataField = dataRecord.nextField();
				System.out.println(dataField);
			}

			System.out.print("\n");

		}

//		InLineFileCsvDataRecordDecoder inLineFileCsvDataRecordDecoder = new InLineFileCsvDataRecordDecoder("C:/Users/patri/git/datarecord/datarecord/resources/inlinedata.txt");
//		
//		DataRecord dataRecordsFromInLineFile = inLineFileCsvDataRecordDecoder.nextRecord();
//		
//		System.out.println(dataRecordsFromInLineFile.toString());

//		FileCsvDataRecordDecoder fileCsvDataRecordDecoder = new FileCsvDataRecordDecoder("C:/Users/patri/git/datarecord/datarecord/resources/data.txt");
//		
//		DataRecord[] dataRecordsFromFile = fileCsvDataRecordDecoder.getDataRecords();
//		
//		for (DataRecord dataRecord : dataRecordsFromFile) {
//			
//			System.out.println(dataRecord.toString());
//			
//		}
//		
//		System.out.println("///////////////////////////////////////");///////////////////////////
//		
//		InLineFileCsvDataRecordDecoder inLineFileCsvDataRecordDecoder = new InLineFileCsvDataRecordDecoder("C:/Users/patri/git/datarecord/datarecord/resources/inlinedata.txt");
//		
//		DataRecord[] dataRecordsFromInLineFile = inLineFileCsvDataRecordDecoder.getDataRecords();
//		
//		for (DataRecord dataRecord : dataRecordsFromInLineFile) {
//			
//			System.out.println(dataRecord.toString());
//			
//			
//		}

	}

}
