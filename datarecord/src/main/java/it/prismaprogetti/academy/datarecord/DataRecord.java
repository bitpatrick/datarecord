package it.prismaprogetti.academy.datarecord;

/**
 * una istanza di DataRecord rappresenta una persona con le propriet� di
 * nome,cognome,et�,citta
 *
 * contentitore di DataField
 * 
 * pu� avere al massimo 4 DataField( quindi 4 componenti chiave=valore)
 * 
 */
public class DataRecord {

	/**
	 * Campi
	 */
	private DataField[] dataFields;

	private int numeroDiRecordRitornatti;

	/**
	 * Inizializzazione con i campi
	 * 
	 * @param dataFields
	 */
	public DataRecord(DataField[] dataFields) {
		super();
		this.dataFields = dataFields;
	}

	/**
	 * Ritorna il campo successivo
	 * 
	 * @return
	 * @throws DataRecordException: viene sollevata nel momento in cui i record sono
	 *                              terminati
	 */
	public DataField nextField() throws DataRecordException {

		if (!this.hasNext()) {
			throw DataRecordException.fieldInesistente();
		}

		DataField dataField = dataFields[this.numeroDiRecordRitornatti++];

		return dataField;

	}

	/**
	 * Ritorna true se esiste il campo successivo
	 * 
	 * @return
	 */
	public boolean hasNext() {

		return !(this.numeroDiRecordRitornatti > this.dataFields.length - 1);

	}

	@Override
	public String toString() {

		String output = "";

		for (DataField dataField : this.dataFields) {
			output += dataField.toString();

		}

		output += "\n";

		return output;

	}

}
