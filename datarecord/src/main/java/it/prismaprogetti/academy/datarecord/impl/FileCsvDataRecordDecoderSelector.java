package it.prismaprogetti.academy.datarecord.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;

import it.prismaprogetti.academy.datarecord.DataRecordException;

public class FileCsvDataRecordDecoderSelector {

	/**
	 * Singleton
	 */
	private static FileCsvDataRecordDecoderSelector instance = new FileCsvDataRecordDecoderSelector();

	/**
	 * Lista dei decoder
	 */
	private List<FileCsvDataRecordDecoder> decoders=new ArrayList<>();

	private FileCsvDataRecordDecoderSelector() {

		/*
		 * Trovo tutte le classi di tipo deocoder e le inserisco come istanze nella
		 * lista
		 */
		Reflections reflections = new Reflections("it.prismaprogetti.academy");

		Set<Class<? extends FileCsvDataRecordDecoder>> decoderClasses = reflections
				.getSubTypesOf(FileCsvDataRecordDecoder.class);
		for (Class<? extends FileCsvDataRecordDecoder> decoderClass : decoderClasses) {
			System.out.println("Aggiunto decoder "+ decoderClass.getName());
			try {
				FileCsvDataRecordDecoder decoder = decoderClass.newInstance();
				decoders.add(decoder);
			} catch (Exception ignored) {
			}
		}
		
	}

	public FileCsvDataRecordDecoder select(String path)
			throws IOException, DataRecordException, NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {

		for (FileCsvDataRecordDecoder decoder : decoders) {

			boolean compatibility = decoder.checkCompatibility(path);
			if (compatibility) {
				decoder.open(path);
				return decoder;
			}
		}

		throw new IllegalArgumentException("decoder non compatibile per il percorso specificato");
	}

	public static FileCsvDataRecordDecoderSelector builder() {
		return instance;
	}

}
