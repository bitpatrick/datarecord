package it.prismaprogetti.academy.datarecord.impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * file del tipo:
 * Nome,Cognome,Eta;Flavio,Giannini,47;Mario,Rossi,22;Letizia,Gialli,34;
 * 
 */
public class InLineFileCsvDataRecordDecoder extends FileCsvDataRecordDecoder {

	@Override
	protected String[] estraiRigheDati(BufferedReader bufferedReader) throws IOException {
		String[] righeDati = bufferedReader.readLine().trim().split(";"); // ;
		return righeDati;
	}

	public boolean checkCompatibility(String path) throws FileNotFoundException, IOException {

		int contatoreDiRighe = 0;

		try (InputStreamReader isr = new FileReader(path); BufferedReader br = new BufferedReader(isr);) {

			while (br.readLine() != null) {

				if (++contatoreDiRighe > 1) {

					return false;

				}

			}

			return true;

		}

	}

}
