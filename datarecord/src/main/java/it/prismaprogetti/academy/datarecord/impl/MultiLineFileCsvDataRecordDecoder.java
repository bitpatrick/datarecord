package it.prismaprogetti.academy.datarecord.impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Decodificatore di file txt del tipo:
 * 
 * Nome,Cognome,Eta Flavio,Giannini,47 Mario,Rossi,22 Letizia,Gialli,34
 */
public class MultiLineFileCsvDataRecordDecoder extends FileCsvDataRecordDecoder {

	@Override
	protected String[] estraiRigheDati(BufferedReader bufferedReader) throws IOException {

		List<String> righeDatiList = new ArrayList<String>();

		String rigaDati = null;
		while ((rigaDati = bufferedReader.readLine()) != null) {
			righeDatiList.add(rigaDati);
		}

		String[] righeDati = righeDatiList.toArray(new String[] {});
		return righeDati;
	}

	public boolean checkCompatibility(String path) throws FileNotFoundException, IOException {

		int contatoreDiRighe = 0;

		try (InputStreamReader isr = new FileReader(path); BufferedReader br = new BufferedReader(isr);) {

			while (br.readLine() != null) {

				if (++contatoreDiRighe > 1) {

					return true;

				}

			}

			return false;

		}

	}

}
