package it.prismaprogetti.academy.datarecord.error;

public class DataFieldException extends Exception{

	private DataFieldException(String message) {
		super(message);
	}
	
	public static DataFieldException chiaveNulla() {
		return new DataFieldException("la chiave � nulla");
	}
	

}
