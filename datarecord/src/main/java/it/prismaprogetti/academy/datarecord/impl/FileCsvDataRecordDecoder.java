package it.prismaprogetti.academy.datarecord.impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.academy.datarecord.DataField;
import it.prismaprogetti.academy.datarecord.DataRecord;
import it.prismaprogetti.academy.datarecord.DataRecordDecoder;
import it.prismaprogetti.academy.datarecord.DataRecordException;

public abstract class FileCsvDataRecordDecoder implements DataRecordDecoder {

	private String path;
	private DataRecord[] dataRecords;
	private int numeroRecordRitornati;
	private boolean open = false;

	void open(String path) throws DataRecordException {
		this.path = path;
		this.open = true;
		this.dataRecords = this.read();
	}

	public DataRecord nextRecord() throws DataRecordException {
		if (!open) {
			throw DataRecordException.decoderNonInizializzato();
		}

		if (!this.hasNext()) {
			throw DataRecordException.recordInesistente();
		}

		DataRecord dataRecord = this.dataRecords[numeroRecordRitornati++];

		return dataRecord;
	}

	public boolean hasNext() throws DataRecordException {
		if (!open) {
			throw DataRecordException.decoderNonInizializzato();
		}

		return !(this.numeroRecordRitornati == this.dataRecords.length);
	}

	private DataRecord[] read() throws DataRecordException {
		if (!open) {
			throw DataRecordException.decoderNonInizializzato();
		}

		/*
		 * variabili usate per creare elenchi di DataRecord
		 */
		List<DataRecord> dataRecordList = new ArrayList<>();
		DataRecord[] dataRecordArray = null; // variabile restituita dal metodo

		/*
		 * variabili usate dall'intestazione
		 */
		String[] arrayIntestazione;
		String[] arrayDati;

		try (
				/*
				 * apro un canale di comunicazione verso la fonte dati
				 */
				InputStreamReader inputStreamReader = new FileReader(this.path);
				/*
				 * ci applico un decoratore: mi aiuta a semplificarmi il lavoro di recupero
				 * righe
				 */
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);) {

			String[] righeDati = estraiRigheDati(bufferedReader);

			/*
			 * intestazione
			 */
			arrayIntestazione = righeDati[0].split(","); // [Nome|Cognome|Eta]

			/*
			 * dati
			 */
			for (int i = 1; i < righeDati.length; i++) {

				String rigaDati = righeDati[i]; // 1) [Flavio,Giannini,47] 2) ...

				List<DataField> dataFields = new ArrayList<>(); // size: elementi che ci sono dentro --- capacity:
																// capacit

				arrayDati = rigaDati.trim().split(",");// Patrizio,Cervigni,34 --> [Patrizio,Cervigni,34]

				/*
				 * eseguo esattamente 4 cicli per ogni arrayDati perch l'array composto da 3
				 * elementi
				 */
				for (int j = 0; j < arrayDati.length; j++) {

					/*
					 * creo field
					 */
					DataField dataField = new DataField(arrayIntestazione[j], arrayDati[j]);

					/*
					 * aggiungo il field alla lista di DataField
					 */
					dataFields.add(dataField);

				}

				/*
				 * creo un data Record che contiene 3 data fields
				 * 
				 * come parametro trasformo una lista in array
				 */
				DataRecord dataRecord = new DataRecord(dataFields.toArray(new DataField[arrayDati.length]));

				/*
				 * aggiungo il data record alla lista
				 */
				dataRecordList.add(dataRecord);

			}

			dataRecordArray = dataRecordList.toArray(new DataRecord[dataRecordList.size()]);

		} catch (IOException e) {
			throw DataRecordException.erroreLetturaDati(e);
		}

		return dataRecordArray;

	}

	protected abstract String[] estraiRigheDati(BufferedReader bufferedReader) throws IOException;
	
	public abstract boolean checkCompatibility(String path) throws FileNotFoundException, IOException;

}
