package it.prismaprogetti.academy.datarecord;

/**
 * Rappresenta un campo all'interno di un DataRecord
 * 
 * @author Flavio Giannini
 *
 */
public class DataField {

	private String chiave;
	private String valore;

	public DataField(String chiave, String valore) {
		super();
		this.chiave = chiave;
		this.valore = valore;
	}

	public String getChiave() {
		return chiave;
	}

	public String getValore() {
		return valore;
	}

	@Override
	public String toString() {
		return "DataField [chiave=" + chiave + ", valore=" + valore + "]";
	}

}
